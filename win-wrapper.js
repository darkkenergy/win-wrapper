/**
 * @name WinWrapper
 * @desc A basic wrapper module for the global Window Object, in order to ease
 *      stubbing the Window Object in unit tests, by use of something like
 *      proxyquire (https://github.com/thlorenz/proxyquire.)
 * 
 * @return {Window}: Returns the global Window Object.
 */
function WinWrapper() {
    return window;
}

module.exports = WinWrapper;